(ns mymensa2openmensa.core
  (:gen-class)
  (:require [net.cgrand.enlive-html :as html]
            [mymensa2openmensa.mensas :as mensas]
            [mymensa2openmensa.days :as days]
            [mymensa2openmensa.meals :as meals]
            [clojurewerkz.urly.core :as urly]
            [clojure.string :refer [join]]
            [mymensa2openmensa.helper :refer [resolve-url]]
            [mymensa2openmensa.resources :refer [*base-url*]]
            [mymensa2openmensa.xml.canteen :refer [generate-document]]
            [mymensa2openmensa.xml.meta :as meta]
            [mymensa2openmensa.helper :refer [feed-url]]
            [clojure.java.io :as io]
            [selmer.parser :as selmer]
            [cheshire.core :as json])
  (:import [java.net URL]))

(defn save-feed-json [mensas]
  (println "Generate feed.json")
  (let [mensa-json (->> mensas
                        (mapv (fn [m]
                                (println (str (select-keys m [:absolute-meta-url :absolute-feed-url])))
                                (vector (str "mymensa2openmensa-" (:mensa-code m))
                                        (:absolute-meta-url m))))
                        (into {})
                        (json/encode))
        path "xml/meta/feed.json"]
    (println "Write feed.json: " path)
    (io/make-parents path)
    (spit path mensa-json)))

(defn save-index-html [mensas]
  (println "Generate Index.html")
  (let [document (selmer/render-file "index.html" {:mensas mensas})
        path "xml/index.html"]
    (println "Write Index.html: " path)
    (io/make-parents path)
    (spit path document))
  )

(defn save-mensa-as-xml [mensa]
  (println "Generate XML for " (:title mensa))
  (let [document (generate-document mensa)
        path (str "xml/feed/" (:mensa-code mensa) ".xml")]
    (println "Write XML Document: " path)
    (io/make-parents path)
    (spit path document)))

(defn save-mensa-meta-xml [mensa & {:keys [feed-path]}]
  (let [document (meta/generate-document mensa :feed-path feed-path)
        path (str "xml/meta/" (:mensa-code mensa) ".xml")]
    (println "Write Meta XML Document: " path)
    (io/make-parents path)
    (spit path document)))

(defn extract-mensas [base-url & {:keys [feed-path]}]
  (binding [*base-url* base-url]
    (println "Searching for Mensas on" base-url)
    (let [mensas (->> (mensas/get-active-mensas)
                      (map
                       #(assoc %
                               :absolute-feed-url
                               (resolve-url (str feed-path "/") (str "./" (:mensa-code %) ".xml"))
                               :absolute-meta-url
                               (resolve-url (str feed-path "/") (str "../meta/" (:mensa-code %) ".xml"))
                               :feed-url (str "./feed/" (:mensa-code %) ".xml")
                               :meta-url (str "./meta/" (:mensa-code %) ".xml"))))]
      (println "Found" (count mensas) "Mensas.")
      (when (pos? (count mensas))
        (doseq [m mensas]
          (save-mensa-as-xml m)
          (save-mensa-meta-xml m :feed-path feed-path)))
      (save-index-html mensas)
      (save-feed-json mensas)
      )))

(defn -main
  "Analyze a given My-Mensa.de Base-URL and extract all Canteens as XML-Files."
  [& args]
  (if-let [base-url (first args)]
    (let [feed-path (second args)]
      (extract-mensas base-url
                      :feed-path feed-path))
    (println "Bitte Base-URL der My-Mensa App angeben.")))
