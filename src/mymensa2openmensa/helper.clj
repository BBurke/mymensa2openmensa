(ns mymensa2openmensa.helper
  (:require [net.cgrand.enlive-html :as html]
            [clojurewerkz.urly.core :as urly])
  (:import [java.util Calendar]
           [java.net URLDecoder]
           [java.time.format DateTimeFormatter]))

(defn date-string [inst]
  (let [pattern "yyyy-MM-dd"
        dtf (DateTimeFormatter/ofPattern pattern)]
    (-> inst
        .toZonedDateTime
        (.format dtf))))

(defn attr-value [data attr]
  (first (html/attr-values data attr)))

(defn digit-string? [string]
  (every? #(Character/isDigit %) string))

(defn absolute-url [base-url url]
  (let [base-url (urly/url-like base-url)]
    (-> base-url
        (.withoutQueryStringAndFragment)
        (.mutatePath url)
        (.toString)
        (URLDecoder/decode))))

(defn date-from-day-number [year day-number]
  (doto (Calendar/getInstance)
    (.set Calendar/HOUR_OF_DAY 0)
    (.set Calendar/MONTH 0)
    (.set Calendar/YEAR year)
    (.set Calendar/DAY_OF_MONTH 1)
    (.add Calendar/DAY_OF_MONTH day-number)))

(defn url-fragment [url]
  (let [base-url (urly/url-like url)]
    (urly/fragment-of base-url)))


(defn feed-url [feed-path mensa-code]
  (str feed-path "/" mensa-code ".xml"))

(defn resolve-url [base-path other-path]
  (urly/resolve base-path other-path))


