(ns mymensa2openmensa.meals
  (:require [net.cgrand.enlive-html :as html]
            [cheshire.core :as json]
            [clojure.string :refer [trim includes? split join replace lower-case blank?]
             :rename {replace replace-string}]
            [mymensa2openmensa.resources :refer [*base-url* cached-html-resource
                                                 get-meals-url get-details-url]]
            [mymensa2openmensa.helper :refer [digit-string? attr-value url-fragment
                                              date-from-day-number date-string]]
            ))


(defn parse-id-date [date]
  (when-let [date-part  (re-find #"\d{4,}" date)]
    (let [year (Integer/parseInt (subs date-part 0 4))
          day-of-year (Integer/parseInt (subs date-part 4))]
      (date-from-day-number year
                            day-of-year))))

(defn parse-meal-link [link]
  (let [url (attr-value link :href)]
    {:title (html/text link)
     :url url}))

(defn parse-price [price-elment]
  (when-let [price (re-find #"\d*,\d*" price-elment)]
    (replace-string price #"," ".")))

(defn parse-prices [day-page]
  (let [price-categories [:student :employee :other]
        price-elements (html/select day-page [:ul (html/lefts
                                                   [:li (html/pred #(includes? (str %) "Preise"))])
                                              [(html/text-pred #(includes? % "€"))]])]
    (into {}
          (mapv vector
                price-categories
                (keep
                 parse-price
                 price-elements)
                ))))

(defn clean-note [note]
  (-> note
      (replace-string #"not found:" "")
      trim))

(defn parse-notes [day-page]
  (map clean-note
       (html/texts (html/select day-page
                                [(html/lefts
                                  [:li (html/pred #(includes? (lower-case (str %)) "kennzeichnungen"))])]))))

(defn parse-meal-label [meal-page]
  (let [meal-label (join " " (map html/text (html/select meal-page [:p.ct])))]
    (if (blank? meal-label)
      (->> (html/texts (html/select meal-page
                                    [[:ul
                                      (html/pred
                                       #(not (or (includes? (lower-case (str %)) "kennzeichnungen")
                                                 (includes? (lower-case (str %)) "deutsch")
                                                 (includes? (lower-case (str %)) "english"))))]
                                     :li]))
           (map clean-note)
           (join ", "))
      meal-label)))

(defn valid-meal? [meal]
  (and (not (blank? (:label meal)))
       (not (blank? (:category meal)))))

(defn parse-day-page [meal-page]
  (when-let [id (attr-value meal-page :id)]
    (let [date (parse-id-date (attr-value meal-page :id))]
      {:id id
       :date date
       :date-string (date-string date)
       :prices (parse-prices meal-page)
       :notes (parse-notes meal-page)
       :category (html/text (first (html/select meal-page [:h4.ct])))
       :label (parse-meal-label meal-page)
       :mensa-label (html/text (first (html/select meal-page [:.mensatitle])))})))

(defn get-all-meals [mensa-code]
  (let [content (cached-html-resource (get-details-url mensa-code))
        meal-pages (html/select content [[:div :.page.details]])]
    (filter valid-meal?
            (map
             parse-day-page
             meal-pages))))


