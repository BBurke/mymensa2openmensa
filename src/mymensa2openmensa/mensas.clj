(ns mymensa2openmensa.mensas
  (:require [net.cgrand.enlive-html :as html]
            [cheshire.core :as json]
            [clojure.string :refer [split blank? trim]]
            [mymensa2openmensa.meals :refer [get-all-meals]]
            [mymensa2openmensa.helper :refer [attr-value absolute-url url-fragment]]
            [mymensa2openmensa.resources :refer [*base-url* get-chooser-url cached-html-resource]]
            [mymensa2openmensa.xml.canteen :as xmlcanteen]
            [clojure.java.io :as io]
            [clojure.edn :refer [read-string]]))

(defn get-mensainfo [mensa-code]
  (let [mensainfo (read-string (slurp (io/resource "mensainfo.edn")))]
    (get mensainfo (keyword mensa-code))))

(defn parse-mensa-link [link]
  (let [mensa-code (first (split (url-fragment (attr-value link :href)) #"_tage"))]
    (when-not (blank? mensa-code)
      (merge
       {:mensa-code mensa-code
        :title (trim (html/text link))
        :gps (json/parse-string (attr-value link :data-gps) true)
        :url (absolute-url *base-url* (attr-value link :href))}
       (get-mensainfo mensa-code)))))

(defn add-meal-days [mensa]
  (assoc mensa
         :days (group-by :date-string (get-all-meals (:mensa-code mensa)))))

(defn get-active-mensas []
  (let [content (cached-html-resource (get-chooser-url))
        mensa-links (html/select  content [:ul#mensalist (html/but :li.ui-disabled) :a])]
    (map
     (comp add-meal-days parse-mensa-link)
     mensa-links)))

(defn get-all-mensas []
  (let [content (cached-html-resource (get-chooser-url))
        mensa-links (html/select  content [:ul#mensalist :li :a])]
    (map
     parse-mensa-link
     mensa-links)))

