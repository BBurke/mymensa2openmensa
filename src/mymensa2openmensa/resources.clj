(ns mymensa2openmensa.resources
  (:require [mymensa2openmensa.helper :refer [absolute-url]]
            [net.cgrand.enlive-html :as html]))


(def ^:dynamic *base-url*)

(defn get-chooser-url []
  (absolute-url *base-url* "/chooser.php?hyp=0&lang=de&mensa=all"))

(defn get-meals-url [mensa-code]
  (absolute-url *base-url* (str "/essen.php?hyp=0&lang=de&mensa=" mensa-code)))

(defn get-details-url [mensa-code]
  (absolute-url *base-url* (str "/details.php?hyp=0&lang=de&mensa=" mensa-code)))

(def cached-html-resource
  (memoize (fn [url]
             (html/html-resource (new java.net.URL url)))))

