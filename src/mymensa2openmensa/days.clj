(ns mymensa2openmensa.days
  (:require [net.cgrand.enlive-html :as html]
            [cheshire.core :as json]
            [clojure.string :refer [includes? split]]
            [mymensa2openmensa.helper :refer [digit-string? attr-value url-fragment date-from-day-number absolute-url]]
            [mymensa2openmensa.resources :refer [*base-url*]]))


(defn parse-fragment-date [fragment-date]
  (when-let [date-fragment-part  (last (split fragment-date #"_"))]
    (when (and (> (count date-fragment-part) 4)
               (digit-string? date-fragment-part))
      {:year (Integer/parseInt (subs date-fragment-part 0 4))
       :day-of-year (Integer/parseInt (subs date-fragment-part 4))})))

(defn parse-day-fragment [fragment]
  (let [parsed-date-string (parse-fragment-date fragment)]
    (when parse-fragment-date
      (date-from-day-number (:year parsed-date-string)
                            (:day-of-year parsed-date-string)))))

(defn parse-day-link [link]
  (let [url (attr-value link :href)]
    {:title (html/text link)
     :dayNumber (parse-day-fragment (url-fragment url))
     :url (absolute-url *base-url* url)}))

(defn is-day-url? [url]
  (includes? url "essen.php"))

(defn is-day-link? [link]
  (when-let [url (attr-value link :href)]
    (is-day-url? url)))

(defn get-all-days [content]
  (let [day-links (html/select  content [:ul :li :a])]
    (->> day-links
         (filter is-day-link?)
         (map parse-day-link))))
