(ns mymensa2openmensa.xml.category
  (:require [clojure.data.xml :refer :all]
            [mymensa2openmensa.xml.meal :refer [meal-tag]]))


(defn category-tag [{:keys [title meals]}]
  (element :category {:name title}
           (map
            #(meal-tag %)
            meals)))
