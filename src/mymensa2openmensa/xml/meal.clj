(ns mymensa2openmensa.xml.meal
  (:require [clojure.data.xml :refer :all]))


(defn meal-tag [meal]
  (element :meal {}
           (element :name {} (:label meal))
           (map (fn [note]
                  (element :note {}
                           note))
                (:notes meal))
           (map (fn [[role price]]
                  (element :price {:role (name role)}
                           price))
                (:prices meal))
           ))
