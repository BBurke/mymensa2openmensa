(ns mymensa2openmensa.xml.day
  (:require [clojure.data.xml :refer :all]
            [mymensa2openmensa.xml.category :refer [category-tag]]))


(defn day-tag [{:keys [date-string meals]}]
  (let [categories (group-by :category meals)]
    (element :day {:date date-string}
             (map
              #(category-tag
                {:title (first %)
                 :meals (second %)})
              categories))))
