(ns mymensa2openmensa.xml.canteen
  (:require [clojure.data.xml :refer :all]
            [mymensa2openmensa.xml.day :refer [day-tag]]
            [mymensa2openmensa.xml.common :refer [openmensa-tag]]
            [mymensa2openmensa.xml.feed :refer [feed-tag]]))

(defn canteen-tag [mensa]
  (element :canteen {}
           (element :name {} (:title mensa))
           (element :location {:latitude (get-in mensa [:gps :lat])
                               :longitude (get-in mensa [:gps :lon])})
           (map #(day-tag {:date-string (first %)
                           :meals (second %)})
                (sort (:days mensa)))
           ))

(defn optional-element [keyname value]
  (element keyname {} value))

(defn time-element [times keyname]
  (when-let [time (get times keyname)]
    (if-not (= :closed time)
      (element keyname {:open time})
      (element keyname {:closed true}))))

(defn mensa-times [times]
  (when times
    (element :times {:type "opening"}
             (map #(time-element times %)
                  [:monday :tuesday :wednesday :thursday :friday :saturday :sunday]))))

(defn canteen-meta-tag [mensa & {:keys [feed-path]}]
  (element :canteen {}
           (element :name {} (:title mensa))
           (optional-element :address (:address mensa))
           (optional-element :city (:city mensa))
           (optional-element :phone (:phone mensa))
           (element :location {:latitude (get-in mensa [:gps :lat])
                               :longitude (get-in mensa [:gps :lon])})
           (mensa-times (:times mensa))
           (feed-tag mensa :feed-path feed-path)))

(defn generate-document [mensa]
  (indent-str (openmensa-tag
               (canteen-tag mensa))))
