(ns mymensa2openmensa.xml.common
  (:require [clojure.data.xml :refer :all]))

(defn openmensa-tag [content]
  (element :openmensa
           {:xmlns "http://openmensa.org/open-mensa-v2"
            :xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance"
            :version "2.1"
            :xsi:schemaLocation "http://openmensa.org/open-mensa-v2 http://openmensa.org/open-mensa-v2.xsd"}
           content))
