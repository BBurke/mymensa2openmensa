(ns mymensa2openmensa.xml.meta
  (:require [clojure.data.xml :refer :all]
            [mymensa2openmensa.xml.canteen :refer [canteen-meta-tag]]
            [mymensa2openmensa.xml.common :refer [openmensa-tag]]))

(defn generate-document [mensa & {:keys [feed-path]}]
  (indent-str (openmensa-tag
               (canteen-meta-tag mensa :feed-path feed-path))))
