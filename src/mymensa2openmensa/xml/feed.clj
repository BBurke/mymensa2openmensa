(ns mymensa2openmensa.xml.feed
  (:require [clojure.data.xml :refer :all]
            [mymensa2openmensa.helper :refer [feed-url]]
            [mymensa2openmensa.resources :refer [get-details-url]]))

;;; TODO allow schedule options as cli parameters
(defn feed-tag [mensa & {:keys [feed-path]}]
  (element :feed {:name "full"}
           (element :schedule {:hour "7-19" :minute "35" :retry "30 2"})
           (element :url {} (feed-url feed-path (:mensa-code mensa)))
           (element :source {} (get-details-url (:mensa-code mensa)))))
