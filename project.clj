(defproject mymensa2openmensa "0.1.0"
  :description "Simple Project for generating Open-Mensa XML Documents from My-Mensa App"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/core.cache "1.0.207"]
                 [enlive "1.1.6"]
                 [cheshire "5.10.0"]
                 [clojurewerkz/urly "1.0.0"]
                 [selmer "1.12.31"]]
  :main ^:skip-aot mymensa2openmensa.core
  :target-path "target/%s"
  :uberjar-name "mymensa2openmensa.jar"
  :profiles {:uberjar {:aot :all}})
