# mymensa2openmensa

This Project analyze My-Mensa Apps and generates XML-Documents for https://openmensa.org/  
This project is configured to run with Gitlab CI and publish the data via Gitlab-Pages.


## Build

To run this Project you need [Leiningen](https://leiningen.org/) and [Java/OpenJDK 8](https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=hotspot) installed. 
Switch to the base directory and run the following to build the uberjar file:

    $ lein uberjar

The uberjar will be created under ``target/uberjar/mymensa2openmensa.jar``  
Or you can run the Application with the following command (see [Section: Options](#options) for parameters):

    $ lein run base-url [feed-path]


## Usage

Run the generated Uberjar with a my-mensa App Base-Url as first parameter.  

    $ java -jar mymensa2openmensa.jar base-url [feed-path]

## Options

*base-url*: Base URL of the specific My-Mensa App e.g. *https://muenster.my-mensa.de*.  
*feed-path*: Optional Parameter to specify the Path where the xml files will be hosted. Necessary to create absolute pathes in index.xml

### Bugs

This is a simple html scraper. A little change on the my-mensa app might break this process.

## License

Copyright © 2020 Bruno Burke

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
